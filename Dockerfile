FROM python:3.7-alpine

RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev openssl-dev make git && \
    apk add --no-cache --update python3 && \
    pip3 install --upgrade pip setuptools

COPY . /app
WORKDIR /app

RUN pip install pipenv
RUN pipenv install --deploy --system --ignore-pipfile

CMD ["python", "app.py"]