import json
import os
import sys
import csv
import elasticsearch
from elasticsearch import helpers
from elasticsearch import Elasticsearch
import logging
import logging.config
import configparser
import re
from tld import get_tld
import calendar
from datetime import datetime
import pandas as pd
import mmap
import subprocess
import time


class ElasticSearchExporter:
    def __init__(self):
        logging.config.fileConfig("logging_config.ini")
        self.config = configparser.RawConfigParser()
        config_read_ok = self.config.read('config.ini')
        if len(config_read_ok) == 0:
            raise Exception('No config.ini file found')

        self.records_per_scroll_page = int(self.config.get('default', 'records_per_scroll_page'))
        self.max_time_to_process_scroll_page = self.config.get('default', 'max_time_to_process_scroll_page')

        self.index_name = os.environ.get(self.config.get('default', 'index_name_env'))
        self.doc_type = "user-state"
        self.es_query_json_file = "kite_query.json"

        self.yearmonth_string = os.environ.get(self.config.get('default', 'yearmonth_env'))

        self.start_timestamp, self.end_timestamp = self.get_month_range()

        self.output_directory = "./outputs"

    def connect_to_es(self, retry_counter):
        try:
            if retry_counter <= 3:
                logging.info("Connecting to ES %d" % retry_counter)
                host = os.environ.get(self.config.get('default', 'es_host_env'))
                if not host:
                    host = "localhost:9200"
                self.es = Elasticsearch(hosts=[host], timeout=60, retry_on_timeout=True)
                self.es.info()
                logging.info("Connected to ES successfully")
                return
        except elasticsearch.ConnectionError as e:
            time.sleep(10)
            self.connect_to_es(retry_counter + 1)

    def get_month_range(self):
        year = int(self.yearmonth_string.split("-")[0])
        month = int(self.yearmonth_string.split("-")[1])
        month_range = calendar.monthrange(year, month)

        start_date_string = self.yearmonth_string + "-" + str(month_range[0])
        end_date_string = self.yearmonth_string + "-" + str(month_range[1])

        start_datetime = datetime.strptime(start_date_string, '%Y-%m-%d').replace(day=1, hour=0, minute=0, second=0)
        end_datetime = datetime.strptime(end_date_string, '%Y-%m-%d').replace(hour=23, minute=59, second=59)

        start_timestamp = self.unix_time_millis(start_datetime)
        end_timestamp = self.unix_time_millis(end_datetime)

        return start_timestamp, end_timestamp

    @staticmethod
    def unix_time_millis(dt):
        epoch = datetime.utcfromtimestamp(0)
        return int((dt - epoch).total_seconds() * 1000)

    @staticmethod
    def mapcount(filename):
        f = open(filename, "r+")
        buf = mmap.mmap(f.fileno(), 0)
        lines = 0
        readline = buf.readline
        while readline():
            lines += 1
        return lines

    def export(self, fields="all", delimiter=","):
        logging.info("Started export")
        output_csv = self.output_directory + "/kite" + self.yearmonth_string + ".csv"
        output_csv_done = self.output_directory + "/kite" + self.yearmonth_string + "_done.csv"

        if os.path.isfile(output_csv_done):
            lines_num = self.mapcount(output_csv_done)
            logging.info("File %s already exists, it has %d rows." % (output_csv_done, lines_num))
            return output_csv_done

        self.connect_to_es(1)

        query = json.load(open(self.es_query_json_file))

        query["query"]["bool"]["must"][0]["range"]["reference_date"]["gte"] = self.start_timestamp
        query["query"]["bool"]["must"][0]["range"]["reference_date"]["lte"] = self.end_timestamp

        # Fetch the mapping in order to create the header
        mapping = self.es.indices.get_mapping(index=self.index_name,
                                              doc_type=self.doc_type)[self.index_name]['mappings'][self.doc_type]['properties'].keys()

        # Set handler to elasticsearch
        scan_response = helpers.scan(self.es,
                                     index=self.index_name,
                                     query=query,
                                     size=self.records_per_scroll_page,
                                     scroll=self.max_time_to_process_scroll_page,
                                     clear_scroll=False,
                                     request_timeout=120)
        # write to file
        counter = 0
        with open(output_csv, 'w') as f:
            if fields == "all":
                w = csv.DictWriter(f, mapping, delimiter=delimiter, extrasaction='ignore', quoting=csv.QUOTE_MINIMAL)
            else:
                fields = fields.split(",")
                w = csv.DictWriter(f, [i for i in mapping if i in fields], delimiter=delimiter,
                                   extrasaction='ignore', quoting=csv.QUOTE_MINIMAL)
            w.writeheader()
            try:
                for row in scan_response:
                    for key, value in row['_source'].items():
                        row['_source'][key] = value
                    _ = w.writerow(row['_source'])
                    counter += 1
            except elasticsearch.exceptions.NotFoundError:
                pass
            except elasticsearch.exceptions.RequestError:
                pass

        os.rename(output_csv, output_csv_done)
        logging.info('%s lines was expotred to file: %s' % (counter, output_csv_done))

        return output_csv_done

    def analyse(self, csv_path):
        df = pd.read_csv(csv_path)

        df['date'] = pd.to_datetime(df['reference_date'], unit='ms')
        df.sort_values(by=['user_guid', 'date'], ascending=[True, True], inplace=True)
        grouped_df = df.groupby('user_guid')[['state', 'date']].aggregate(lambda x: list(x)).reset_index()

        print(df.shape)

        grouped_df['first_state'] = grouped_df['state'].str[0]
        grouped_df['second_state'] = grouped_df['state'].str[1]
        grouped_df['last_state'] = grouped_df['state'].str[-1]
        grouped_df['went_cold'] = grouped_df['last_state'] == 'cold'
        grouped_df['new'] = (grouped_df['first_state'] == 'new') | (grouped_df['second_state'] == 'new')
        grouped_df['resurrected'] = (grouped_df['first_state'] == 'resurrected') & (grouped_df['second_state'] != 'new')
        grouped_df['active_from_last_month'] = (grouped_df['resurrected'] == False) & (grouped_df['new'] == False)
        grouped_df['went_cold_from_signup'] = (grouped_df['went_cold'] == True) & (grouped_df['new'] == True)
        grouped_df['went_cold_from_resurrected'] = (grouped_df['resurrected'] == True) & (grouped_df['last_state'] == 'cold')
        grouped_df['went_cold_from_active_last_month'] = (grouped_df['active_from_last_month'] == True) & (grouped_df['went_cold'] == True)
        grouped_df['remained_active_from_last_month'] = (grouped_df['active_from_last_month'] == True) & (grouped_df['went_cold'] == False)

        logging.info("New = %d" % grouped_df[grouped_df['new'] == True].shape[0])
        logging.info("Resurrected = %d" % grouped_df[grouped_df['resurrected'] == True].shape[0])
        logging.info("Wend Cold Total = %d" % grouped_df[grouped_df['went_cold'] == True].shape[0])
        logging.info("Wend Cold From signup = %d" % grouped_df[grouped_df['went_cold_from_signup'] == True].shape[0])
        logging.info("Wend Cold From resurrected = %d" % grouped_df[grouped_df['went_cold_from_resurrected'] == True].shape[0])
        logging.info("Wend Cold From active last month = %d" % grouped_df[grouped_df['went_cold_from_active_last_month'] == True].shape[0])
        logging.info("Remained Active = %d" % grouped_df[grouped_df['remained_active_from_last_month'] == True].shape[0])


if __name__ == "__main__":
    exporter = ElasticSearchExporter()

    exported_csv = exporter.export()

    exporter.analyse(exported_csv)
